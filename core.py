import PySimpleGUI as sg
from get_ip import get_ip_number


# Layout
sg.theme("DefaultNoMoreNagging")

layout = [
    [sg.Text("Endereço IP", font=("Arial", 15), justification="right")],
    [sg.Push()],
    [sg.Text(get_ip_number(), key="status_text", font=("Arial", 20), justification="center")],
    [sg.Push()],
    [sg.Button("Recarregar", size=(20,3))],
    [sg.VPush()],
    [sg.Text("Sinobras © 2022", font=("Arial", 10))],
]


# Create the window
window = sg.Window("SINOBRAS - MEU IP",
                   layout,
                   size=(300,200),
                   finalize=True)

while True:
    event, values = window.read()
    if event in (sg.WIN_CLOSED, "Cancel"):
        break
    if event == "Recarregar":
        window["status_text"].update(get_ip_number())


window.close()