import socket

def get_ip_number():

    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        ip_number = s.getsockname()[0]
        print(ip_number)
        return ip_number
    except IOError as e:
        if e.errno == 101:
            status = "Sem conexão com a Internet"
            print(status)
            return status

get_ip_number()
